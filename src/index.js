import express from "express";
import bodyParser from "body-parser";

import { messagesRouter } from "./messages.js";

const hostname = "127.0.0.1";
const port = 3000;

const app = express();

// parse JSON in body requests
app.use(bodyParser.json());

app.get("/", function (req, res) {
  res.send("hello world");
});

app.get("/demo", function (req, res) {
  res.status(200).send({ message: "Ceci est une démo !" });
});

app.get("/error", function (req, res) {
  res.status(500).send({ error: "Something broke!" });
});

app.use("/api", messagesRouter);

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

// make json beautifier
app.set("json spaces", 2);
