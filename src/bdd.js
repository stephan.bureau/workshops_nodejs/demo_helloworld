import { promisify } from "util";
import mysql from "mysql";

export class BDD {
  /** @type {import("mysql").Connection} */
  connection;

  constructor(config) {
    this.connection = mysql.createConnection(config);
  }
  /**
   * Make a SQL query
   * @param {string} sql
   * @param {any} args
   * @return {Promise<import("mysql").QueryFunction>}
   */
  query(sql, args = {}) {
    return promisify(this.connection.query).call(this.connection, sql, args);
  }

  /**
   * Close BDD connection
   * @returns {Promise<void>}
   */
  close() {
    return promisify(this.connection.end).call(this.connection);
  }
}
