import express from "express";
import moment from "moment";
import { BDD } from "./bdd.js";

export const messagesRouter = express.Router();

const bddConfig = {
  host: "localhost",
  user: "admin",
  password: "admin",
  database: "demo",
};

// define the home page route
messagesRouter.get("/", async (req, res) => {
  const bdd = await new BDD(bddConfig);

  const result = await bdd.query("SELECT * from messages");

  await bdd.close();

  res.status(200).send(result);
});

// create a new message in database
messagesRouter.post("/", async (req, res) => {
  /** @type {{ message: string, date: string }} */
  const { message, date = "" } = req.body;

  // if message is not sent as parameter
  if (!message) {
    res.status(400).send('"message" parameter not found!');
  }

  const dateModified = (date ? moment(date) : moment()).format(
    "YYYY-MM-DD HH:mm:ss"
  );

  const bdd = new BDD(bddConfig);

  try {
    const result = await bdd.query(
      `INSERT INTO messages (message, date) VALUES ("${message}", "${dateModified}");`
    );
    await bdd.close();
    res.status(200).send(result);
  } catch (err) {
    await bdd.close();
    res.status(500).send(err);
  }
});
